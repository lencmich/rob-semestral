#!/usr/bin/python

import numpy as np

def get_transform_matrix():
    with open('calib.npy', 'rb') as f:
        H = np.load(f)

    return H

def save_backup_matrix():
    H = np.array([[-2199.5, -26.4, 1073.6],
                 [-29.6, 2206.7, -911.9], [0, 0, 1]])

    with open('calib.npy', 'wb') as f:
        np.save(f, H)

def camera_robot_transform(A, B):
    global H
    # A... image coords matrix 2 x N,
    # B... robot coords matrix 2 x N

    #H = np.array([[-2202.2, -39.2, 1079.2],
    #             [-31.6, 2149.4, -878.7], [0, 0, 1]])
    #H = np.array([[-2239.7, -22.4, 1074.1],
    #             [-22.1, 2246.6, -927.6], [0, 0, 1]])
    #H = np.array([[-2210.2, -26.1, 1072.0],		bad on left side
    #             [-26.1, 2234.5, -921.2], [0, 0, 1]])
    #H = np.array([[-2200.7, -24.1, 1074.3],		much better
    #             [-28.4, 2203.6, -905.2], [0, 0, 1]])
    H = np.array([[-2199.5, -26.4, 1073.6],
                 [-29.6, 2206.7, -911.9], [0, 0, 1]])

    # Make the cords holonom by adding 3. row of ones
    B_new = np.append(B, [np.ones(len(B[1]))], axis=0)
    A_new = np.append(A, [np.ones(len(A[1]))], axis=0)

    # Transpose the matrixes
    A_t = A_new.T
    B_t = B_new.T

    U = np.array(A[0])
    V = np.array(A[1])

    # Get parts of the array
    h1 = np.matmul(U.T, np.linalg.pinv(B_new))
    h2 = np.matmul(V.T, np.linalg.pinv(B_new))
    h3 = np.matmul(np.ones(len(A_t)), np.linalg.pinv(B_new))

    # This is a kind of cheating and yeah, I feel bad about this, but why not
    h3[0] = 0
    h3[1] = 0

    # Stack the array together
    H = np.vstack((h1, h2, h3))

    with open('calib.npy', 'wb') as f:
        np.save(f, H)

if __name__ == '__main__':
    A = np.array([[294.8038253, 501.37061448, 721.32792928,  941.92070693, 284.11253223,  501.04175672,
          723.69796774,  947.5804353 , 280.28916085,  504.80300472,  726.71317918,  952.09977333,
          281.21998405,  507.7110596 ,  729.9636997 ,  953.91910699],
         [736.10930803,  741.9131843 ,  746.66695505,  748.77824639, 517.20834336,  521.57677267,
          525.03554275,  526.61881081, 295.62583527,  298.49044508,  300.51006311,  302.73841072,
          72.54772405,   76.52553881,   77.72600601,   80.45102719]])
    B = np.array([[0.35,  0.25,  0.15,  0.05,  0.35,  0.25,  0.15,  0.05,  0.35,0.25,  0.15,  0.05,  0.35,  0.25,  0.15,  0.05],
        [0.75,  0.75,  0.75,  0.75,  0.65,  0.65,  0.65,  0.65,  0.55, 0.55,  0.55,  0.55,  0.45,  0.45,  0.45,  0.45]])
    camera_robot_transform(A, B)
    print(get_transform_matrix())
