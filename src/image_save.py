#! /usr/bin/python

import rospy
import cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from numpy import pi
from image_prepare import image_process

# robot interface
# from mitsubishi_arm_student_interface.mitsubishi_robots import Mitsubishi_robot

# Provides
bridge = CvBridge()

img_received = False


def image_callback(msg):
    img = bridge.imgmsg_to_cv2(msg, "bgr8")
    cv2.imwrite('camera_image.jpeg', img)
    img_received = True


def get_image():

    image_topic = "/camera/image_color"

    rospy.Subscriber(image_topic, Image, image_callback)
    while not rospy.is_shutdown():
        msg = rospy.wait_for_message(image_topic, Image, timeout=10)
        if msg:
            return cv2.imread('camera_image.jpeg')

    # return cv2.imread('camera_image.jpeg')


if __name__ == '__main__':
    rospy.init_node('image_listener')
    img = get_image()
    objects = image_process(img)
    print(objects)
