from image_save import get_image
from image_prepare import image_process, Object, AllObjects
from transform import camera_robot_transform, get_transform_matrix, save_backup_matrix
import numpy as np
from numpy import pi
import cv2
import rospy
import copy
import operator

from mitsubishi_arm_student_interface.mitsubishi_robots import Mitsubishi_robot

gripper_vals = ['open', 0.055, 0.036]
#calib_vals = [[0, 0.6, 0.08], [0.4, 0.5, 0.08], [
#    0.2, 0.7, 0.08], [0.1, 0.6, 0.08], [0.4, 0.7, 0.08]]
#calib_vals = [[0.45, 0.75, 0.08], [0.35, 0.75, 0.08], 
#    [0.25, 0.75, 0.08], [0.15, 0.75, 0.08], [0.05, 0.75, 0.08], [-0.05, 0.75, 0.08],
#    [0.45, 0.65, 0.08], [0.35, 0.65, 0.08], [0.25, 0.65, 0.08], [0.15, 0.65, 0.08],
#    [0.05, 0.65, 0.08], [-0.05, 0.65, 0.08], [0.45, 0.55, 0.08], [0.35, 0.55, 0.08], [0.25, 0.55, 0.08],
#    [0.15, 0.55, 0.08], [0.05, 0.55, 0.08], [-0.05, 0.55, 0.08], [0.45, 0.45, 0.08], [0.35, 0.45, 0.08], 
#    [0.25, 0.45, 0.08], [0.15, 0.45, 0.08], [0.05, 0.45, 0.08], [-0.05, 0.45, 0.08]]
calib_vals = [[0.45, 0.75, 0.08], [0.45, 0.45, 0.08], [0.15, 0.65, 0.08], [-0.05, 0.75, 0.08], [-0.05, 0.45, 0.08]]
calib_matrix = []

if __name__ == '__main__':
    robot = Mitsubishi_robot()
    robot.set_max_speed(0.3)

    B = []
    for i in range(len(calib_vals)):
        B.append(calib_vals[i][0:2])

    B = np.array(B)
    B = np.transpose(B)

    print(B)

    robot.set_joint_values([pi/2, pi/2, 0, 0, 0, 0])

    robot.set_gripper(gripper_vals[0])

    while (raw_input('write "h" to continue: ') != "h"):
        continue

    robot.set_gripper(gripper_vals[1])

    while (raw_input('write "ok" to continue: ') != 'ok'):
        continue

    for i in range(len(calib_vals)):
        robot.set_joint_values([pi/2, 0, pi/2, 0, pi/2, 0])

        coords = robot.get_position()
        coords[0] = calib_vals[i][0]
        coords[1] = calib_vals[i][1]
        coords[2] = calib_vals[i][2]
        J = robot.ikt(coords)
        found_res = False
        for res in J:
            if (res[1] < pi/2):
               robot.set_joint_values(res)
               found_res = True
               break
        
        if not found_res:
            print("Can not move with object. IKT exists but it interferes with objects.\n")
            continue 

        robot.set_gripper('open')

        J_old = copy.deepcopy(res)

        coords[2] = calib_vals[i][2] + 0.2

        J = robot.ikt(coords)
        for res in J:
            if (res[1] < pi/2):
               robot.set_joint_values(res)
               found_res = True
               break
        
        if not found_res:
            print("Can not move with object. IKT exists but it interferes with objects.\n")
            continue 
        J_old1 = copy.deepcopy(res)
        robot.set_joint_values([0, 0, pi/2, 0, pi/2, 0])

        img = get_image()
        objects = image_process(img, 1)
        for obj_col in objects:
            for obj in obj_col:
                if obj.obj_shape in ["cylinder", "cube"]:
                    print(obj_col)
                    calib_matrix.append([obj.obj_coord[0],
                                         obj.obj_coord[1]])

        robot.set_joint_values(J_old1)
        robot.set_joint_values(J_old)
        robot.set_gripper(gripper_vals[1])

    robot.set_joint_values([0, 0, pi/2, 0, pi/2, 0])

    robot.set_gripper('open')

    A = calib_matrix
    A = np.array(A)
    A = np.transpose(A)
    print("points A\n", A, "\npoints B\n", B)

    if (len(A) != len(B)):
        print("ERROR: Number of calib points", len(B) ,"does not match number of image points", len(A))
        print("Switching to backup matrix...")
        save_backup_matrix()
    else:
        camera_robot_transform(A, B)

    #R, t = rigid_transform_2D(A, B)
    #print("R= \n", R, "\nt = \n", t)

    H = get_transform_matrix()

    # Just test if H is correct
    A_new = np.append(A, [np.ones(len(A[1]))], axis=0)
    test = np.matmul(np.linalg.inv(H), A_new[:, 0].T)
    print("result is:\n", test)

    robot.set_joint_values([pi/2, 0, pi/2, 0, pi/2, 0])
