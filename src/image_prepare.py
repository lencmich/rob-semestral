import cv2
import numpy as np
import math
import sys

IMG_PATH = 'zpracovani_obrazu/'
IMG = '02.jpeg'
DIST_PATH = 'img_dist/'
DIST_IMG = 'dist.jpg'
SHOW_IMG = False


class Object:
    def __init__(self, obj_shape, obj_size, obj_coord, obj_area, obj_lng, obj_angle):
        self.obj_shape = obj_shape
        self.obj_size = obj_size
        self.obj_coord = obj_coord
        self.obj_area = obj_area
        self.obj_lng = obj_lng
        self.obj_angle = obj_angle
        self.obj_rob_coord = None

    def __repr__(self):
        ret_str = '\tObjekt: \n'
        ret_str += '\t\tTvar: ' + str(self.obj_shape) + ' \n'
        ret_str += '\t\tVelikost: ' + str(self.obj_size) + ' \n'
        ret_str += '\t\tSouradnice (cam):' + str(self.obj_coord) + ' \n'
        ret_str += '\t\tSouradnice (rob):' + str(self.obj_rob_coord) + ' \n'
        ret_str += '\t\tUhel (rad): ' + str(self.obj_angle) + ' \n'
        ret_str += '\t\tObsah:' + str(self.obj_area) + ' \n'
        ret_str += '\t\tObvod:' + str(self.obj_lng)
        return ret_str

    def __str__(self):
        return self.__repr__()

    def get_hom_coords(self):
        return [self.obj_coord[0], self.obj_coord[1], 1]

    def save_rob_coord(self, coord):
        '''
        Input: Tuple of robot coord (rx, ry)
        '''
        self.obj_rob_coord = coord


class AllObjects:
    def __init__(self):
        self.objects = []

    def __repr__(self):
        ret_str = ''
        for obj in self.objects:
            ret_str += str(obj) + '\n'
        return ret_str

    def __str__(self):
        return self.__repr__()

    def __len__(self):
        return len(self.objects)

    def __getitem__(self, i):
        return self.objects[i]

    def add_item(self, item):
        self.objects.append(item)

    def get_cubes(self):
        ret_cubes = []
        for obj in self.objects:
            if obj.obj_shape == 'cube' and obj.obj_size != None:
                ret_cubes.append(obj)
        return ret_cubes

    def get_cylinders(self):
        ret_cylinders = []
        for obj in self.objects:
            if obj.obj_shape == 'cylinder' and obj.obj_size != None:
                ret_cylinders.append(obj)
        return ret_cylinders

    def sort_objects(self):
        cub = self.get_cubes()
        cyl = self.get_cylinders()
        self.objects = cub + cyl

    def __sub_sort(self, objects):
        '''
        Classic Bubble Sort.
        '''
        for i in range(len(objects)-1):
            for j in range(0, len(objects)-i-1):
                if objects[j].obj_size > objects[j+1].obj_size:
                    objects[j], objects[j+1] = objects[j+1], objects[j]

    def transform_to_robot_coord(self, H):
        '''
        Transform from camera to robot coords.

        Input:  Transformation matrix H.
                self.coord of all object

        Output: None
        '''
        for obj in self.objects:
            coords = np.transpose(obj.get_hom_coords())
            coords = np.matmul(np.linalg.inv(H), coords.T)
            obj.save_rob_coord((coords[0], coords[1]))


def color_filter(img, palette):
    results = []
    i = 0
    for color in palette:
        if 240 < sum(color) / 3 or sum(color) / 3 < 20:  # detection of black or white
            continue
        mask = cv2.inRange(img, color - 10, color + 10)
        im = cv2.bitwise_and(img, img, mask=mask)
        kernel = np.ones((5, 5), 'uint8')
        #dilate_img = cv2.erode(im, kernel, iterations=2)
        #dilate_img = cv2.dilate(dilate_img, kernel, iterations=5)
        dilate_img = cv2.morphologyEx(im, cv2.MORPH_OPEN, kernel)
        dilate_img = cv2.morphologyEx(dilate_img, cv2.MORPH_CLOSE, kernel)
        cv2.imwrite(DIST_PATH + str(i) + "_" + DIST_IMG, dilate_img)
        results.append(dilate_img)
        i += 1

    return results


def find_parameters(img):
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(img_gray, 10, 255, 0)
    contours = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    allObjects = AllObjects()

    for obj in contours[0]:
        area = cv2.contourArea(obj)
        if area < 3000:  # skip small objects
            continue
        lng = cv2.arcLength(obj, True)
        comp = lng ** 2 / area

        if 15 < comp < 18:
            obj_type = 'cube'
            if 5000 < area < 9500:
                obj_size = 2
            elif 12000 < area < 22000:
                obj_size = 1
            elif 26000 < area < 40000:
                obj_size = 0
            else:
                obj_size = None
        elif 11 < comp < 15:
            obj_type = 'cylinder'
            if 4000 < area < 8000:
                obj_size = 2
            elif 10000 < area < 20000:
                obj_size = 1
            elif 22000 < area < 37000:
                obj_size = 0
            else:
                obj_size = None
        else:
            obj_type = 'unknown'
            obj_size = None

        M = cv2.moments(obj)
        cx = (M['m10'] / M['m00'])
        cy = (M['m01'] / M['m00'])

        angle = cv2.minAreaRect(obj)[2]
        if (angle < -45):
            angle = angle + 90
        angle = (angle  * np.pi) / 180 # convert angle to radians

        objClass = Object(obj_type, obj_size, (cx, cy), area, lng, angle)
        print(objClass)
        allObjects.add_item(objClass)

        # Print information about object into image
        img_cont = cv2.drawContours(
            img, contours[0], -1, (0, 0, 255), thickness=3)
        font = cv2.FONT_HERSHEY_SIMPLEX
        center = (int(cx), int(cy))
        center2 = (int(cx), int(cy+30))
        fontScale = 1
        fontColor = (255, 255, 255)
        thickness = 1
        lineType = 2
        cv2.putText(img_cont, obj_type + str(center),
                    center,
                    font,
                    fontScale,
                    fontColor,
                    thickness,
                    lineType
                    )
        cv2.putText(img_cont, str(angle)[0:5],
                    center2,
                    font,
                    fontScale,
                    fontColor,
                    thickness,
                    lineType
                    )
        cv2.circle(img_cont, center, 5, (255, 0, 0), -1)
        cv2.imwrite(
            DIST_PATH +
            'cont_{:d}'.format(hash(allObjects)) + DIST_IMG, img_cont
        )

    return allObjects


def image_process(img, K):
    img_norm = cv2.normalize(img, None, 0, 255, cv2.NORM_MINMAX)
    if SHOW_IMG:
        cv2.imshow("Image", img)
        cv2.waitKey(0)

    img_gray = cv2.cvtColor(img_norm, cv2.COLOR_BGR2GRAY)

    img_gray2 = cv2.cvtColor(img_norm, cv2.COLOR_BGR2HSV)
    img_gray2 = cv2.cvtColor(cv2.bitwise_not(img_gray2), cv2.COLOR_BGR2GRAY)
    invGamma = 1.2
    table = np.array([((i / 255) ** invGamma) *
                     255 for i in np.arange(0, 256)]).astype("uint8")
    # img_gray2 = cv2.LUT(1-img_gray2, table)
    if SHOW_IMG:
        cv2.imshow("Gray1 Image", img_gray)
        cv2.waitKey(0)
        cv2.imshow("Gray2 Image", img_gray2)
        cv2.waitKey(0)

    cv2.imwrite(DIST_PATH + "gray1_" + DIST_IMG, img_gray)
    cv2.imwrite(DIST_PATH + "gray2_" + DIST_IMG, img_gray2)

    _, img_segmented1 = cv2.threshold(img_gray, 120, 255, cv2.THRESH_BINARY)
    _, img_segmented2 = cv2.threshold(img_gray2, 105, 255, cv2.THRESH_BINARY)

    if SHOW_IMG:
        cv2.imshow("SEG1 Image", img_segmented1)
        cv2.waitKey(0)
        cv2.imshow("SEG2 Image", img_segmented2)
        cv2.waitKey(0)

    img_segmented = cv2.bitwise_and(img_segmented1, img_segmented2)
    cv2.imwrite(DIST_PATH + "seg1_" + DIST_IMG, img_segmented1)
    cv2.imwrite(DIST_PATH + "seg2_" + DIST_IMG, img_segmented2)
    kernel = np.ones((5, 5), 'uint8')
    img_segmented = cv2.dilate(img_segmented, kernel, iterations=2)
    img_segmented = cv2.erode(img_segmented, kernel, iterations=1)
    if SHOW_IMG:
        cv2.imshow("Segmented Output Image", img_segmented)
        cv2.waitKey(0)

    kernel = (25, 25)
    img = cv2.GaussianBlur(img, kernel,0)

    # Add the masked foreground and background
    outImage = cv2.add(img, cv2.merge(
        (img_segmented, img_segmented, img_segmented)))
    if SHOW_IMG:
        cv2.imshow("Output Image", outImage)
        cv2.waitKey(0)

    Z = outImage.reshape((-1, 3))
    # convert to np.float32
    Z = np.float32(Z)
    # define criteria, number of clusters(K) and apply kmeans()
    criteria = (cv2.TERM_CRITERIA_EPS, 20, 1.0)
    K = K + 1 # add background color
    ret, label, center = cv2.kmeans(
        Z, K, None, criteria, 20, cv2.KMEANS_PP_CENTERS)
    # Now convert back into uint8, and make original image
    center_u8 = np.uint8(center)
    res = center_u8[label.flatten()]
    img_knn = res.reshape((img.shape))

    cv2.imwrite(DIST_PATH + "in_" + DIST_IMG, img)
    cv2.imwrite(DIST_PATH + "seg_" + DIST_IMG, img_segmented)
    cv2.imwrite(DIST_PATH + "out_" + DIST_IMG, outImage)
    cv2.imwrite(DIST_PATH + "knn_" + DIST_IMG, img_knn)

    img_found = color_filter(img_knn, center)

    objects = []
    for obj in img_found:
        parameters = find_parameters(obj)
        objects.append(parameters)

    if SHOW_IMG:
        cv2.imshow('res2', img_knn)
        cv2.waitKey(0)

    return objects

if __name__ == '__main__':
    img = cv2.imread(IMG_PATH + IMG)
    print(str(sys.argv))
    obj = image_process(img, int(sys.argv[1]))

    i = 0
    for o in obj:
        o.sort_objects()
        print('Barva {}:\n'.format(i) + str(o))
        i += 1
