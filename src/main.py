from image_save import get_image
from image_prepare import image_process, Object, AllObjects
from transform import get_transform_matrix
import numpy as np
from numpy import pi
import cv2
import rospy
import copy
import sys

from mitsubishi_arm_student_interface.mitsubishi_robots import Mitsubishi_robot

gripper_vals = ['open', 0.055, 0.035]
height = [0.078, 0.058, 0.038]
take_height = [0.08, 0.08]

def sort_size(obj_type):
    ret_obj = AllObjects()

    for i in range(0, 3):
        for obj in obj_type:
            # check size of the object
            if obj.obj_size == i:
                ret_obj.add_item(obj)

    return ret_obj

def move_objects(obj_sorted):
    base = []
    for obj in obj_sorted:
        if not base:
            base = [obj.obj_rob_coord[0],
                    obj.obj_rob_coord[1], height[obj.obj_size], obj.obj_angle]
            print("base = ", base)
            print("Not moving with object\n", obj, "because it is used as a base.\n")
            base_coords = []
            base_coords = robot.get_position()
            base_coords[0] = base[0]
            base_coords[1] = base[1]
            base_coords[2] = base[2] + 0.2
            #base_coords[4] = abs(base[3])
            print(base_coords)
            J = robot.ikt(base_coords)
            if not J:
                print("Base\n", base_coords, "can not be reached by the robot. Skipping.\n")   
                return 0

            continue

        if base and (obj.obj_size == 0):
            # This one cannot be pick up by the robot becuse of its size and cannot be used as
            # a base since we already have one
            print("Can not move with object\n", obj, "because of its big size.\n")
            continue

        obj_coords = [obj.obj_rob_coord[0],
                        obj.obj_rob_coord[1], take_height[obj.obj_size-1], obj.obj_angle]
        print("coords = ", obj_coords)

        coords = []
        coords = robot.get_position()
        coords[0] = obj_coords[0]
        coords[1] = obj_coords[1]
        coords[2] = obj_coords[2] + 0.2      # get above the object
        #coords[4] = abs(obj_coords[3])
        J = robot.ikt(coords)
        if not J:
            print("Can not move with object\n", obj, "because IKT does not exist.\n")
            continue
        
        J[0][5] = J[0][5] + obj_coords[3]

        robot.set_gripper('open')

        # Move robot above the object
        robot.set_joint_values(J[0])
        print(coords)
    	#while (raw_input('write "ok" to continue: ') != 'ok'):
        #    continue

        J_old = copy.deepcopy(J[0])

        coords[2] = obj_coords[2]
        J = robot.ikt(coords)
        
        found_res = False
        for res in J:
            if (res[1] < pi/2):
               res[5] = res[5] + obj_coords[3]
               robot.set_joint_values(res)
               found_res = True
               break
        
        if not found_res:
            print("Can not move with object\n", obj, "IKT exists but it interferes with objects.\n")
            continue

        # Grab the object
        robot.set_gripper(gripper_vals[obj.obj_size])

        J_old[5] = J_old[5] - obj_coords[3]
        robot.set_joint_values(J_old)

        #robot.set_joint_values([pi/2, 0, pi/2, 0, pi/2, 0])

        base_coords = robot.get_position()
        base_coords[0] = base[0]
        base_coords[1] = base[1]
        base_coords[2] = base[2] + 0.2      # get above the object
        #base_coords[4] = abs(base[3])

        J = robot.ikt(base_coords)
        J[0][5] = J[0][5] + base[3]

        J_old = copy.deepcopy(J[0])

        # Move robot above base coords
        robot.set_joint_values(J[0])

        base_coords[2] = base[2] + obj_coords[2]
        print(base)

        # increment coord z in base
        base[2] = base[2] + height[obj.obj_size]
        print(base)
    	#while (raw_input('write "ok" to continue: ') != 'ok'):
        #    continue

        J = robot.ikt(base_coords)
        found_res = False
        for res in J:
            if (res[1] < pi/2):
               res[5] = res[5] + base[3]
               robot.set_joint_values(res)
               found_res = True
               break
        
        if not found_res:
            print("Can not move with object\n", obj, "IKT exists but it interferes with objects.\n")
            break       

        # Put the object on the base and leave it there
        robot.set_gripper('open')

        J_old[5] = J_old[5] - base[3]

        robot.set_joint_values(J_old)


if __name__ == '__main__':
    robot = Mitsubishi_robot()
    robot.set_max_speed(0.3)

    if (len(sys.argv) != 2):
        print("ERROR: There should be 1 command line argument for number of colors, ", len(sys.argv) , "were given!")
        exit(0)

    # Get transform matri
    H = get_transform_matrix()

    robot.set_joint_values([0, 0, pi/2, 0, pi/2, 0])

    # while (raw_input('write "ok" to continue: ') != 'ok'):
    #       continue

    # Get and process the image
    img = get_image()
    objects = image_process(img, int(sys.argv[1]))
    i = 0
    for o in objects:
        o.sort_objects()
        print('Barva {}:\n'.format(i) + str(o))
        i += 1

    robot.set_joint_values([pi/2, 0, pi/2, 0, pi/2, 0])

    # For each color
    for obj_col in objects:
        print("new color")

        # Transform image coords to robot coords
        obj_col.transform_to_robot_coord(H)

        # Sort objects based on their size
        #obj_col.sort_objects()
        obj_col = sort_size(obj_col)

        # And split it to cubes and cylynders
        obj_cube = obj_col.get_cubes()
        obj_cyl = obj_col.get_cylinders()

        print(obj_cube)

        # Move cubes
        move_objects(obj_cube)

        print(obj_cyl)

        # Move cylynders
        move_objects(obj_cyl)

        print("All objects has been moved.\n")
